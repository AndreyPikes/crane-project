using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraneLimitSound : MonoBehaviour
{
    private static AudioSource sound;

    private void Start()
    {
        sound = GetComponent<AudioSource>();
    }

    public static void Play()
    {        
        if (!sound.isPlaying) sound.Play();
    }
}
