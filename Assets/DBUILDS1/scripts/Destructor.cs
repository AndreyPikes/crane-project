using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OverlapSphere
{
	public LayerMask layerMaskDestruct;//destruct
	public LayerMask layerMaskFracChunks;//chunks
	public Vector3 size = new Vector3(10, 2, 20);
	public Vector3 offset = new Vector3(0, 1, 10);
	public float radius = 2.5f;
}


public class Destructor : MonoBehaviour 
{
	public FracPieces fracPieces;
	int x;
	public OverlapSphere oBox = new OverlapSphere();//overlap sphere
	/// <summary>
	/// Все попавшие в зону касания по тегу коллайдеры
	/// </summary>
	private Collider[] overlapingColiders;
	private Collider[] hitCollFrac;
	public string dbuildTag = "dbuild";//destructible wall tag [for collision]
	public string chunksTag = "chunks";//chunks tag [for collision]
	private Transform hitCollix;
	int overlapingCollidersChildCount;
	
	private Transform fracx;
	public float exploForce = 100;

	//public bool doDebug;
	int i,y;

	bool isExpoding;
	bool destruct;

	private AudioSource sensor;

    private void Start()
    {
		sensor = GetComponent<AudioSource>();
		
		overlapingColiders = new Collider[0];
		hitCollFrac = new Collider[0];		
	}

	void FixedUpdate()
	{
		//hitColl = Physics.OverlapSphere (transform.TransformPoint (oBox.offset), oBox.radius, oBox.layerMask);			
		//overlapingColiders = Physics.OverlapBox(transform.TransformPoint(oBox.offset), oBox.size, Quaternion.identity, oBox.layerMaskDestruct);
		overlapingColiders = Physics.OverlapSphere(transform.TransformPoint(oBox.offset), oBox.radius, oBox.layerMaskDestruct);

		if (!isExpoding && overlapingColiders.Length != 0) 
		{
			isExpoding = true;

			for (i = 0; i < overlapingColiders.Length; i++) 
			{				
				{
					hitCollix = overlapingColiders[i].transform.GetChild(0).transform;

					if (hitCollix.name == "frac")
					{
						fracx = hitCollix;
						fracx.transform.parent = fracPieces.wallChunks;
						fracx.gameObject.SetActive(true);		
					}    
				}
				overlapingColiders[i].gameObject.SetActive(false);
			}
		}
		Explode();
		//destruct = false;
	}

	private void Explode() //fracture get explo
	{
		//new overlapshere to define affected chunks
		hitCollFrac = Physics.OverlapSphere(transform.TransformPoint(oBox.offset), oBox.radius, oBox.layerMaskFracChunks);
		//hitCollFrac = Physics.OverlapBox (transform.TransformPoint (oBox.offset), oBox.size, Quaternion.identity, oBox.layerMaskFrac);

		for (i = 0; i < hitCollFrac.Length; i++) 
		{
			Destroy(hitCollFrac[i].gameObject, UnityEngine.Random.Range(1, 5));
			Rigidbody rb = hitCollFrac[i].transform.GetComponent<Rigidbody>();
			//rb.isKinematic = false;
			//rb.useGravity = true;
			rb.AddExplosionForce (exploForce, transform.position, oBox.radius);
			

			//add to shedule list meshcombine
			
			//hitCollFrac[i].transform.parent = fracPieces.FreeParts;
			//fracPieces.allFreeParts.Add(rb);
		}
		isExpoding = false;
	}
} 