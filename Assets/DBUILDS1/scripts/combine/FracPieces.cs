﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FracPieces : MonoBehaviour 
{
    //public Transform chunksRoot;
    public Transform wallChunks;
    //public Transform FreeParts;//first all chunks get here
    //public int cx;

    public Transform StaticPartsAfterFall;//then chunks that don't move get here and get combined

    private bool scenePlaying = true;
    //container for fractured pieces
    //waitin to be combined   

    //[HideInInspector]
    //public List<Rigidbody> allFreeParts = new List<Rigidbody>();
    //public List<float> startT = new List<float>();

    //public List<Transform> combinedT = new List<Transform>();    

    [Space(10)]
    public int checkPeriod = 2;

    private int countFrac = 0;

    private void Start()
    {
        //StartCoroutine(Optimizer());
        StartOptimizer();
    }

    private async void StartOptimizer()
    {
        while (scenePlaying)
        {
            countFrac = wallChunks.childCount;
            if (countFrac > 0) await Optimize();
            else await Task.Delay(checkPeriod * 1000);

        }
    }

    private async Task Optimize()
    {
        for (int i = 0; i < countFrac; i++)
        {
            var childFrac = wallChunks.GetChild(i);

            for (int y = 0; y < childFrac.childCount; y++)
            {
                var childChunk = childFrac.GetChild(y);
                if (childChunk.transform.position.y < 0.0f)
                {
                    Debug.Log("Set static");
                    childChunk.gameObject.isStatic = true;
                    childChunk.parent = StaticPartsAfterFall;
                }
                else if (childChunk.transform.position.y < 2.0f)
                {
                    Debug.Log("Destroy");
                    Destroy(childChunk.gameObject, UnityEngine.Random.Range(1, 5));
                }
            }
        }

        await Task.Delay(checkPeriod * 1000); 

        //await Task.Yield();
    }

    private void OnDestroy()
    {
        scenePlaying = false;
    }




    //private IEnumerator Optimizer()
    //{
    //    while (true)
    //    {
    //        var countFrac = wallChunks.childCount;
    //        if (countFrac > 0)
    //        {
    //            for (int i = 0; i < countFrac; i++)
    //            {
    //                var childFrac = wallChunks.GetChild(i);

    //                for (int y = 0; y < childFrac.childCount; y++)
    //                {
    //                    var childChunk = childFrac.GetChild(y);
    //                    if (childChunk.transform.position.y < 1.0f)
    //                    {
    //                        Destroy(childChunk.gameObject, 3f);
    //                        Debug.Log("optimizer works");
    //                        //allFreeParts[i].gameObject.SetActive(false);
    //                        //allFreeParts[i].useGravity = false;
    //                        //allFreeParts[i].isKinematic = true;
    //                        //allFreeParts[i].transform.parent = StaticPartsAfterFall;//move to other folder                        
    //                        //allFreeParts[i].transform.gameObject.isStatic = true;

    //                    }
    //                }


    //            }
    //        }
    //        yield return new WaitForSeconds(checkPeriod);
    //    }

    //}
}


