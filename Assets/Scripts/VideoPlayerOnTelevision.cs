using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

public class VideoPlayerOnTelevision : MonoBehaviour
{
    public UnityEvent VideoEnds;
    private bool isPlaying = false;
    private VideoPlayer videoPlayer;
    private AudioSource videoSound;

    void Start()
    {
        videoSound = GetComponent<AudioSource>();
        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath,
            "video1.mp4");
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, videoSound); //�������� ����������� ����� prepare!
        
    }

    private void Update()
    {
        if (isPlaying && !videoPlayer.isPlaying)
        {
            isPlaying = false;
            VideoEnds?.Invoke();
        }
    }

    public void PlayVideo()
    {
        videoPlayer.Prepare();
        videoSound.Play();
        videoPlayer.Play();
       
        isPlaying = true;
    }
}
