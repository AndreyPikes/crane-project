using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class HandPresence : MonoBehaviour
{
    /// <summary>
    /// ���������� ���������� ������ ����
    /// </summary>
    public bool showController = false;
    
    public InputDeviceCharacteristics controllerCharacteristics;
    public List<GameObject> controllerPrefabs;
    public GameObject handModelPrefab;

    private InputDevice targetDevice;
    private GameObject spawnedController;
    private GameObject spawnedHandModel;
    private Animator handAnimator;

    void Awake()
    {
        TryInitialize();
    }

    void TryInitialize()
    {
        List<InputDevice> devices = new List<InputDevice>();        
        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);

        if (devices.Count > 0)
        {
            targetDevice = devices[0];
            if (showController)
            {
                GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name);
                if (prefab)
                {
                    spawnedController = Instantiate(prefab, transform);
                }
                else
                {
                    Debug.LogError("�� ������� �������������� ������ �����������");
                    spawnedController = Instantiate(controllerPrefabs[0], transform);
                }
            }
            else spawnedHandModel = Instantiate(handModelPrefab, transform);
            handAnimator = spawnedHandModel.GetComponent<Animator>();
        }
    }

    void UpdateHandAnimationByInputs()
    {
        if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            handAnimator.SetFloat("Trigger", triggerValue);
        }
        else handAnimator.SetFloat("Trigger", 0);

        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            handAnimator.SetFloat("Grip", gripValue);
        }
        else handAnimator.SetFloat("Grip", 0);
    }


    void Update()
    {
        if (!targetDevice.isValid)
        {
            TryInitialize();            
        }
            
        else
        {            
            if (!showController) UpdateHandAnimationByInputs();

            //if (targetDevice.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButtonValue) && primaryButtonValue)
            //{
            //    XRController xr = GameObject.Find("RightHand").GetComponent<XRController>();
            //    if (xr)
            //    {
            //        xr.SendHapticImpulse(1, 1);
            //        Debug.Log("������ � ������");
            //    }
            //}                

            //if (targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue) && triggerValue > 0.1f)
            //{
            //    Debug.Log($"������� ����� {triggerValue}");
            //}
            //if (targetDevice.TryGetFeatureValue(CommonUsages.primary2DAxis, out Vector2 primary2DAxisValue) && primary2DAxisValue != Vector2.zero)
            //{
            //    Debug.Log($"�������� ����� {primary2DAxisValue}");
            //}
        }        
    }
}
