using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class ContinuousMovement : MonoBehaviour
{
    [SerializeField]
    private Volume postProcess;    
    public XRNode inputSource;
    public float speed = 1;

    public bool isVignetteEnabled = true;
    private Vignette vignette;
    private Vector3 direction;

    private XRRig rig;
    private Vector2 inputAxis;
    private CharacterController character;
    
    void Start()
    {
        character = GetComponent<CharacterController>();
        rig = GetComponent<XRRig>();
        postProcess.profile.TryGet(out vignette);
    }


    void Update()
    {
        //InputDevices.GetDevicesWithCharacteristics
        InputDevice device = InputDevices.GetDeviceAtXRNode(inputSource);
        device.TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis);

        if (isVignetteEnabled) Vignette();
    }

    private void FixedUpdate()
    {
        Quaternion headYaw = Quaternion.Euler(0, rig.cameraGameObject.transform.eulerAngles.y, 0);
        direction = headYaw * new Vector3(inputAxis.x, 0, inputAxis.y);
        character.Move(direction * Time.fixedDeltaTime * speed);
    }

    private void Vignette()
    {
        float intensity = Mathf.Lerp(vignette.intensity.value * 0.95f, direction.magnitude, Time.fixedDeltaTime * 5);
        vignette.intensity.value = intensity;        
    }
}
