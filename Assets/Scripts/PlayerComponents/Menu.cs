using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [HideInInspector]
    public bool isOpen = false;
    [SerializeField]
    private GameObject components;
    [SerializeField]
    private Toggle toggleSounds;
    [SerializeField]
    private Toggle toggleMotionSickness;
    [SerializeField]
    private Toggle toggleTeleportation;

    private Player player;

    public void ShowMenu(Player player)
    {
        isOpen = true;
        components.SetActive(true);
        this.player = player;
    }

    public void HideMenu()
    {
        isOpen = false;
        components.SetActive(false);
    }

    public void ExitApplication()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BlockTeleportation()
    {        
        if (toggleTeleportation.isOn) player.LocomotionController.isBlocked = false;
        else player.LocomotionController.isBlocked = true;
    }

    public void BlockAudio()
    {
        if (toggleSounds.isOn)
        {
            AudioListener.volume = 1;
        }
            
        else
        {
            AudioListener.volume = 0;
        }
    }

    public void BlockAntiMotionSickness()
    {
        if (toggleMotionSickness.isOn)
        {
            player.ContinuousMovement.isVignetteEnabled = true;
        }

        else
        {
            player.ContinuousMovement.isVignetteEnabled = false;
        }
    }
}

