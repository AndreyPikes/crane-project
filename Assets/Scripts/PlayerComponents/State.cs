using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public abstract class State
{
    protected Player player;
    protected StateMachine stateMachine;

    protected bool previousPrimary = true;
    protected bool previousSecondary = true;
    

    protected State(Player player, StateMachine stateMachine)
    {
        this.player = player;
        this.stateMachine = stateMachine;
    }

    public virtual void Enter()
    {
        Debug.LogWarning($"EnterState {this}");
        //DisplayOnUI(UIManager.Alignment.Left);
    }

    public virtual void InputMenu()
    {
        bool menuPrimary, menuSecondary;
        InputDevice device = InputDevices.GetDeviceAtXRNode(player.sceneMenuArm);
        device.TryGetFeatureValue(CommonUsages.primaryButton, out menuPrimary);
        device.TryGetFeatureValue(CommonUsages.secondaryButton, out menuSecondary);
        if ((menuPrimary && !previousPrimary) || (menuSecondary && !previousSecondary)) 
        {
            if (player.menu.isOpen)
            {
                stateMachine.ChangeState(player.stateOnGround);
                
            }
            else 
            {
                stateMachine.ChangeState(player.stateBlockedInMenu);
                
            }

        }
        previousPrimary = menuPrimary || menuSecondary;
        previousSecondary = menuSecondary || menuPrimary;
    }

    public virtual void LogicUpdate()
    {

    }

    public virtual void PhysicsUpdate()
    {

    }

    public virtual void Exit()
    {

    }

    //protected void DisplayOnUI(UIManager.Alignment alignment)
    //{
    //    UIManager.Instance.Display(this, alignment);
    //}
}
