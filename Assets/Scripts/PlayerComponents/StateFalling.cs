using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFalling : State
{
    private float fallingSpeed = 0;
    private CharacterController character;
    public StateFalling(Player player, StateMachine stateMachine) : base(player, stateMachine)
    {
        character = player.character;
    }

    public override void Enter()
    {
        base.Enter();
        fallingSpeed = 0;
        player.DisableAllControl();

    }

    public override void InputMenu()
    {
        base.InputMenu();

    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        Vector3 rayStart = player.transform.TransformPoint(character.center);
        float raycastSphereRadius = character.radius * 2f;
        float rayLength = character.center.y + 0.01f - raycastSphereRadius;
        bool hasHit = Physics.SphereCast(rayStart, raycastSphereRadius, Vector3.down, out RaycastHit hitInfo, rayLength);

        if (hasHit && hitInfo.transform.gameObject.layer == player.groundLayer)
        {
            stateMachine.ChangeState(player.stateOnGround);
        }

        if (hasHit && hitInfo.transform.gameObject.layer == player.craneLayer)
        {
            stateMachine.ChangeState(player.stateOnCrane);
        }
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        fallingSpeed += player.gravity * Time.fixedDeltaTime;
        character.Move(Vector3.up * fallingSpeed * Time.fixedDeltaTime);
    }

    public override void Exit()
    {
        base.Exit();
    }
}
