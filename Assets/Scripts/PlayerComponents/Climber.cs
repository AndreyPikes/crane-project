using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Climber : MonoBehaviour
{
    public Action armStartHanging;
    public Action armStopHanging;

    public static XRController climbingHand;
    private CharacterController character;
    private bool isHangingPrevious = false;

    private void Start()
    {
        character = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        if (climbingHand)
        {
            Climb();
            if (!isHangingPrevious) armStartHanging?.Invoke();
            isHangingPrevious = true;
        }
        else
        {
            if (isHangingPrevious) armStopHanging?.Invoke();
            isHangingPrevious = false;
        }
    }

    private void Climb()
    {
        InputDevices.GetDeviceAtXRNode(climbingHand.controllerNode).TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 velocity);

        character.Move(transform.rotation * -velocity * Time.fixedDeltaTime);
    }
}
