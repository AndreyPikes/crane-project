using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;

public class Player : MonoBehaviour
{
    [SerializeField] private XRRayInteractor rayInteractor; //�������������� ����� � ���������    
    [SerializeField] private Transform playerOnCraneTransform;
    [SerializeField] private Transform playerDefaultParent;
    [SerializeField] private XRPlayerInteractor leftHandGrabing;
    [SerializeField] private XRPlayerInteractor rightHandGrabing;

    public Menu menu;

    public XRNode sceneMenuArm;    

    //������
    public StateMachine sm;
    public StateBlockedWhileLifting stateBlockedWhileLifting;
    public StateBlockeInMenu stateBlockedInMenu;
    public StateFalling stateFalling;
    public StateOnCrane stateOnCrane;
    public StateOnGround stateOnGround;    

    //������������� �������
    private TeleportationProvider teleportationProvider; //�������� ���������� ��������
    private LocomotionControl locomotionController;    //�������� � ��������� ������������ ���������
    private DeviceBasedSnapTurnProvider deviceBasedSnapTurnProvider;
    private Climber climber;
    private ContinuousMovement continuousMovement; //�� ����������� ��� ���������� climber    

    public ContinuousMovement ContinuousMovement => continuousMovement;
    public LocomotionControl LocomotionController => locomotionController;
    
    [Space]
    public CharacterController character; 
    private XRRig rig;
    public float additionalHeight = 0.1f;
    public float gravity = -6.81f;
    public int groundLayer = 7;
    public int craneLayer = 13;

    private void Awake()
    {
        teleportationProvider = GetComponent<TeleportationProvider>();
        locomotionController = GetComponent<LocomotionControl>();
        deviceBasedSnapTurnProvider = GetComponent<DeviceBasedSnapTurnProvider>();
        climber = GetComponent<Climber>();
        continuousMovement = GetComponent<ContinuousMovement>();

        character = GetComponent<CharacterController>();
        rig = GetComponent<XRRig>();
    }

    void Start()
    {
        sm = new StateMachine();
        stateBlockedWhileLifting = new StateBlockedWhileLifting(this, sm);
        stateBlockedInMenu = new StateBlockeInMenu(this, sm);
        stateFalling = new StateFalling(this, sm);
        stateOnCrane = new StateOnCrane(this, sm);
        stateOnGround = new StateOnGround(this, sm);
        DisableAllControl();
        sm.Initialize(stateFalling);

        menu.HideMenu();

        climber.armStartHanging += () => sm.ChangeState(stateBlockedWhileLifting);
        climber.armStopHanging += () => sm.ChangeState(stateFalling);

        //XRSettings.eyeTextureResolutionScale = 1.5f;
        //QualitySettings.antiAliasing = 2;

        //transform.position = playerOnCraneTransform.position;
        //sm.ChangeState(stateOnCrane);
    }

    void Update()
    {
        sm.CurrentState.InputMenu();
        sm.CurrentState.LogicUpdate();
    }

    public void SetCraneParent()
    {
        transform.SetParent(playerOnCraneTransform);
    }

    public void SetDefaultParent()
    {
        transform.SetParent(playerDefaultParent);
    }

    private void FixedUpdate()
    {
        sm.CurrentState.PhysicsUpdate();
        CapsuleFollowHeadSet();
    }

    private void CapsuleFollowHeadSet()
    {
        character.height = rig.cameraInRigSpaceHeight + additionalHeight;
        Vector3 capsuleCenter = transform.InverseTransformPoint(rig.cameraGameObject.transform.position);
        character.center = new Vector3(capsuleCenter.x, character.height / 2 + character.skinWidth, capsuleCenter.z);
    }



    public void DisableAllControl()
    {
        teleportationProvider.enabled = false;
        locomotionController.enabled = false;        
        deviceBasedSnapTurnProvider.enabled = false;
        //climber.enabled = false;
        continuousMovement.enabled = false;
        rayInteractor.enabled = false;
    }

    public void DisableGrabing()
    {        
        leftHandGrabing.enabled = false;
        rightHandGrabing.enabled = false;
    }

    public void DisableTeleportation()
    {
        teleportationProvider.enabled = false;
        locomotionController.enabled = false;
    }

    

    public void RayInteractorEnabled()
    {
        rayInteractor.enabled = true;
    }

    

    public void LiftPlayer()
    {
        sm.ChangeState(stateBlockedWhileLifting);
        StartCoroutine(LiftPlayerCoroutine());
    }

    public void EnableControlOnCrane()
    {
        deviceBasedSnapTurnProvider.enabled = true;
        continuousMovement.enabled = true;
        climber.enabled = true;
        leftHandGrabing.enabled = true;
        rightHandGrabing.enabled = true;
    }

    public void EnableAllControll()
    {
        teleportationProvider.enabled = true;
        locomotionController.enabled = true;
        deviceBasedSnapTurnProvider.enabled = true;
        climber.enabled = true;
        continuousMovement.enabled = true;
        rayInteractor.enabled = true;
        leftHandGrabing.enabled = true;
        rightHandGrabing.enabled = true;
    }

    IEnumerator LiftPlayerCoroutine()
    {
        climber.enabled = false;
        float startTime = Time.time;
        while (transform.position.y <= playerOnCraneTransform.position.y-0.9f)
        {
            transform.position = Vector3.Lerp(transform.position, playerOnCraneTransform.position, Mathf.Pow((Time.time - startTime), 2) / 15000);
            //this.transform.rotation = Quaternion.Lerp(startRot, Quaternion.Euler(0, 0, -90), Mathf.Pow((Time.time - startTime), 2) * speed);
            yield return null;
        }
        sm.ChangeState(stateOnCrane);
    }
}
