using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateOnGround : State
{
    private CharacterController character;

    public StateOnGround(Player player, StateMachine stateMachine) : base(player, stateMachine)
    {
        character = player.character;
    }

    public override void Enter()
    {
        base.Enter();
        //���� ������� ����
        player.EnableAllControll();
    }

    public override void InputMenu()
    {
        base.InputMenu();        
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        Vector3 rayStart = player.transform.TransformPoint(character.center);
        float raycastSphereRadius = character.radius * 2f;
        float rayLength = character.center.y + 0.01f - raycastSphereRadius;
        bool hasHit = Physics.SphereCast(rayStart, raycastSphereRadius, Vector3.down, out RaycastHit hitInfo, rayLength);

        if (hasHit)
        {
            if (hitInfo.transform.gameObject.layer != player.groundLayer)
            {
                stateMachine.ChangeState(player.stateFalling);
            }
        }
        else stateMachine.ChangeState(player.stateFalling);
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void Exit()
    {
        base.Exit();
    }
}
