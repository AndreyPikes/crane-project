using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateBlockedWhileLifting : State
{
    public StateBlockedWhileLifting(Player player, StateMachine stateMachine) : base(player, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        player.DisableAllControl();
    }

    public override void InputMenu()
    {
        ////!
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void Exit()
    {
        base.Exit();
        
    }
}
