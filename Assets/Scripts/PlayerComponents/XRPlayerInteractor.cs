using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRPlayerInteractor : XRDirectInteractor
{
    protected override void OnHoverEntered(HoverEnterEventArgs args)
    {
        args.interactor.allowSelect = true;
        base.OnHoverEntered(args);
    }
}
