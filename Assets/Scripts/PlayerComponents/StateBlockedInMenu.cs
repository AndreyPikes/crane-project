using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateBlockeInMenu : State
{
    public StateBlockeInMenu(Player player, StateMachine stateMachine) : base(player, stateMachine)
    {
    }

    public override void Enter()
    {
        base.Enter();
        player.DisableGrabing();
        player.RayInteractorEnabled();
        player.menu.ShowMenu(player);
    }

    public override void InputMenu()
    {
        base.InputMenu();

    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void Exit()
    {
        base.Exit();
        player.menu.HideMenu();
    }
}
