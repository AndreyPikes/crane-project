using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class LedderOnPressTrigger : PressTrigger
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<XRRig>() != null) OnPressAction();
    }
}
