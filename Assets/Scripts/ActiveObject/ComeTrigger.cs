using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public abstract class ComeTrigger : MonoBehaviour
{
    public virtual event Action comeAction;

    public virtual void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<XRRig>() != null) comeAction?.Invoke();
    }
}
