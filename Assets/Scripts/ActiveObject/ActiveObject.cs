using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActiveObject : MonoBehaviour
{
    [SerializeField] protected UnityEvent onActivateEvent;
    [SerializeField] protected UnityEvent onComeEvent;
    [SerializeField] protected UnityEvent onPressEvent;
    [SerializeField] protected UnityEvent onExitEvent;
    [Space]
    [SerializeField] protected Canvas onActivateMessage;
    [SerializeField] protected Canvas onComeMessage;
    [SerializeField] protected Material warningMaterial;
    [SerializeField] protected ComeTrigger comeTriggerObject;
    [SerializeField] protected PressTrigger pressTriggerObject;

    protected Material defaultMaterial;
    protected Renderer render;

    public virtual void Start()    
    {
        render = GetComponent<Renderer>();     
        defaultMaterial = render.material;
        render.enabled = true;
        comeTriggerObject.comeAction += OnCome;
        pressTriggerObject.pressAction += OnPress;
    }

    public virtual void OnActivate() 
    {
        SetWarningMaterial();
        onActivateMessage.enabled = true;
        onActivateEvent?.Invoke();
    }
    public virtual void OnCome() 
    {
        comeTriggerObject.comeAction -= OnCome;
        SetDefaultMaterial();
        onActivateMessage.enabled = false;
        onComeMessage.enabled = true; 
        onComeEvent?.Invoke();
    }
    public virtual void OnPress() 
    {
        pressTriggerObject.pressAction -= OnPress;
        onComeMessage.enabled = false; 
        onPressEvent?.Invoke();
    }
    public virtual void OnExit() 
    { 
        onExitEvent?.Invoke();
        SetDefaultMaterial();
        onActivateMessage.enabled = false;
        onComeMessage.enabled = false;
    }


    public void SetWarningMaterial()    
    {
        render.material = warningMaterial;
    }

    public void SetDefaultMaterial()    
    {
        render.material = defaultMaterial;
        
    }

}
