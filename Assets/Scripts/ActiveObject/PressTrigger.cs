using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PressTrigger : MonoBehaviour
{
    public virtual event Action pressAction;

    public virtual void OnPressAction()
    {        
        pressAction?.Invoke();
    }
}