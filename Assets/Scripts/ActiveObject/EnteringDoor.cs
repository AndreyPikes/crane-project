using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnteringDoor : MonoBehaviour
{
    private AudioSource openSound;

    private void Start()
    {
        openSound = GetComponent<AudioSource>();

    }
    public void Rotate()
    {
        StartCoroutine(RotateCoroutine());
    }

    IEnumerator RotateCoroutine()
    {
        float angle = 0;
        openSound.Play();
        while (transform.eulerAngles.y < 30f)
        {
            angle += 0.3f;
            transform.rotation = Quaternion.Euler(0, angle, 0);
            yield return new WaitForEndOfFrame();
        }
    }
}
