using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    public Material warningMaterial;
    private Material defaultMaterial;
    private Renderer render;
    // Start is called before the first frame update
    void Start()
    {
        render = GetComponent<Renderer>();
        defaultMaterial = render.material;
        render.enabled = true;
        
    }

    public void SetWarningMaterial()
    {
        render.material = warningMaterial;
    }

    public void SetDefaultMaterial()
    {
        render.material = defaultMaterial;
    }

}
