using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmetBehaviour : MonoBehaviour
{
    public Transform rotatingTransform;
    private bool isFlipped = false;
    public void HelmetFlip()
    {
        if (!isFlipped)
        {
            rotatingTransform.localRotation = Quaternion.Euler(
            rotatingTransform.localRotation.x,
            rotatingTransform.localRotation.y,
            rotatingTransform.localEulerAngles.z + 180);
            isFlipped = true;
        }
        
    }
}
