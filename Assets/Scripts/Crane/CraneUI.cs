using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraneUI : MonoBehaviour
{   
    [SerializeField]
    private Slider sliderCarriage;

    [SerializeField]
    private Slider sliderHook;

    private CraneMoving _craneMoving;

    private void Start()
    {
        _craneMoving = GetComponent<CraneMoving>();
        sliderCarriage.minValue = 0;
        sliderCarriage.maxValue = _craneMoving.CarriageMaxValue;

        sliderHook.minValue = 0;
        sliderHook.maxValue = _craneMoving.HookBottomValue;
    }

    private void FixedUpdate()
    {
        sliderCarriage.value = _craneMoving.CarriageCurrentValue;
        sliderHook.value = _craneMoving.HookCurrentValue;
    }
}

