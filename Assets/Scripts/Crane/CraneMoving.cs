using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;

public class CraneMoving : MonoBehaviour
{
    [SerializeField] Transform rotatingPart;
    [Space]
    [SerializeField] Transform carriage;
    [SerializeField] Transform carriageLowLimit; //z
    [SerializeField] Transform carriageMaxLimit;
    [Space]
    [SerializeField] Rigidbody hookRigidBody;
    [SerializeField] Transform hook;
    [SerializeField] Transform hookUpperLimit; //z
    [SerializeField] Transform hookBottomLimit;
    [Space]
    [SerializeField] HingeJoint LeverUpDown;
    [SerializeField] HingeJoint LeverPushPull;
    [SerializeField] HingeJoint LeverLeftRight;
    
    public UnityEvent carriageLowLimitEvent;
    public UnityEvent carriageMaxLimitEvent;
    public UnityEvent hookUpperLimitEvent;
    public UnityEvent hookBottomLimitEvent;

    [field: SerializeField]
    public float CarriageMaxValue { get; private set; }
    [field: SerializeField]
    public float CarriageCurrentValue { get; private set; }
    [field: SerializeField]
    public float HookBottomValue { get; private set; }
    [field: SerializeField]
    public float HookCurrentValue { get; private set; }

    private float _hookMaxMass;
    private float _hookCurrentMass;

    private void Awake()
    {
        CarriageMaxValue = (carriageLowLimit.position - carriageMaxLimit.position).magnitude;
        HookBottomValue = (hookUpperLimit.position - hookBottomLimit.position).magnitude;
        _hookMaxMass = hookRigidBody.mass;
    }
    
    private void FixedUpdate()
    {
        RotateCrane(LeverLeftRight.angle, 5f);
        LiftHook(LeverUpDown.angle, 5f);
        MoveCarriage(LeverPushPull.angle, 5f);

        CarriageCurrentValue = (carriageLowLimit.position - carriage.position).magnitude;
        HookCurrentValue = (hookUpperLimit.position - hook.position).magnitude;

        CalculateHookMass();
    }

    private void CalculateHookMass()
    {
        _hookCurrentMass = HookCurrentValue / HookBottomValue * _hookMaxMass;
        hookRigidBody.mass = _hookCurrentMass;
    }

    private void RotateCrane(float input, float threshold)
    {
        if (input > threshold) 
            rotatingPart.Rotate(0, (input - threshold) * 0.005f, 0); //R
        if (input < -threshold) 
            rotatingPart.Rotate(0, (input - threshold) * 0.005f, 0); //L
    }

    private void LiftHook(float input, float threshold)
    {
        if (input > threshold)
        {
            if (hook.localPosition.z < hookUpperLimit.localPosition.z)
                hook.position = Vector3.Lerp(hook.position, hook.position + hook.forward, (input - threshold) * 0.005f); //�� ���� (���� �������) - ���������
            else 
                hookUpperLimitEvent?.Invoke();
        }
            
        if (input < -threshold) //������������� ���� ��������
        {
            if (hook.localPosition.z > hookBottomLimit.localPosition.z)
                hook.position = Vector3.Lerp(hook.position, hook.position - hook.forward, (-input - threshold) * 0.005f);
            else
                hookBottomLimitEvent?.Invoke();
        }
    }

    private void MoveCarriage(float input, float threshold)
    {
        
        if (carriageLowLimit.position.z < carriageMaxLimit.position.z) 
        {
            if (input > threshold)
            {
                if (carriage.position.z > carriageLowLimit.position.z)
                    carriage.position = Vector3.Lerp(carriage.position, carriage.position - carriage.up, (input - threshold) * 0.005f); //�� ���� (���� �������) - � ����
                else
                    carriageLowLimitEvent?.Invoke();
            }

            if (input < -threshold)
            {
                if (carriage.position.z < carriageMaxLimit.position.z)
                    carriage.position = Vector3.Lerp(carriage.position, carriage.position + carriage.up, (-input - threshold) * 0.005f);
                else
                    carriageMaxLimitEvent?.Invoke();

            }
        }
        else
        {
            if (input > threshold)
            {
                if (carriage.position.z < carriageLowLimit.position.z)
                    carriage.position = Vector3.Lerp(carriage.position, carriage.position - carriage.up, (input - threshold) * 0.005f); //�� ���� (���� �������) - � ����
                else
                    carriageLowLimitEvent?.Invoke();
            }

            if (input < -threshold)
            {
                if (carriage.position.z > carriageMaxLimit.position.z)
                    carriage.position = Vector3.Lerp(carriage.position, carriage.position + carriage.up, (-input - threshold) * 0.005f);
                else
                    carriageMaxLimitEvent?.Invoke();

            }
        }
    }
}
