using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class String : MonoBehaviour
{
    [SerializeField] private Transform startPoint;
    [SerializeField] private Transform endPoint;
    [SerializeField] private float correction;

    
    void FixedUpdate()
    {
        transform.position = 0.5f * startPoint.position + 0.5f * endPoint.position;
        transform.localScale = new Vector3(transform.localScale.x, (startPoint.position - endPoint.position).magnitude * correction, transform.localScale.z);        
        transform.rotation = Quaternion.FromToRotation(-Vector3.up, startPoint.position - endPoint.position);
    }
}
