using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRLeverGrabInteractable : XRGrabInteractable
{
    private Transform handVisualisationTransform;
    private Transform handInputTransform;
    private XRDirectInteractor interActor; 
    
    protected override void OnSelectEntering(SelectEnterEventArgs args)
    {
        base.OnSelectEntering(args);
        
        interActor = args.interactor as XRDirectInteractor;
        handInputTransform = args.interactor.transform;
        handVisualisationTransform = args.interactor.GetComponentInChildren<HandPresence>().transform;
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);
        handVisualisationTransform.localPosition = Vector3.zero;
        handVisualisationTransform = null;
        handInputTransform = null;
        interActor.allowSelect = true;
    }

    private void Update()
    {
        if (handVisualisationTransform != null && colliders.Count > 0)
        {
            handVisualisationTransform.position = colliders[0].transform.position;
        }
        if (handInputTransform != null && (handInputTransform.position - colliders[0].transform.position).magnitude > 0.3f)
        {
            SelectExitEventArgs args = new SelectExitEventArgs();
            args.isCanceled = true;

            handInputTransform = null;
            interActor.selectExited.Invoke(args);
            interActor.allowSelect = false;
        }
    }
}
